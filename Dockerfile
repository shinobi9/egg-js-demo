FROM node:14.15.0-alpine AS install

WORKDIR /deps

COPY package*.json ./

RUN npm config set registry https://registry.npm.taobao.org && npm i -g npm@7.0.6 && npm i --production

FROM node:14.15.0-alpine

WORKDIR /app

COPY --from=install /deps/node_modules node_modules

COPY . .

# RUN ls

RUN npm config set registry https://registry.npm.taobao.org && npm i npm && npm run tsc

EXPOSE 7001

CMD npm run prod