import { Application } from 'egg';

export default (app: Application) => {
  const { controller, router } = app;

  router.get('/', controller.home.index);
  router.get('/hello', ctx => {
    ctx.body = `
    <h1>hello ts egg</h1>
    `;
  });
};
